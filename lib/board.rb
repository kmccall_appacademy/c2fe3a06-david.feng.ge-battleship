class Board
  default_grid = Array.new(10) {Array.new(10)}

  def initialize(input_grid = default_grid)
    @grid = input_grid
  end

  attr_accessor :grid

  def count

  end

  def empty?

  end

  def full?

  end

  def place_random_ship

  end

  def won?

  end
  
end
